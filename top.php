<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="keywords" content="<?= isset($PageKeywords) ? $PageKeywords : "my, cool, keywords" ?>"/>
    <meta name="description" content="<?= isset($PageDescription) ? $PageDescription : "My Company gives quality service and expert advice on These cool products. Call us now. Tel:(000) 000-0000" ?>"/>
    <meta name="author" content="Roan Fourie">
    <meta name="robots" content="index, follow"/>
    <link rel="icon" href="../favicon.ico">

    <title><?= isset($PageTitle) ? $PageTitle : "My Website"?></title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="./assets/js/script.js"></script>

    <!-- Bootstrap core CSS -->
    <!-- https://getbootstrap.com/docs/3.3/examples/theme/ -->
    <link href="./assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="./assets/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="./assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="theme.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet" type="text/css">

    <link rel="canonical" href="<?= isset($PageURL) ? $PageURL : "http://localhost:81/" ?>" />

<!--      ╦═╗╔═╗╔═╗╔╗╔  ╔═╗╔═╗╦ ╦╦═╗╦╔═╗
          ╠╦╝║ ║╠═╣║║║  ╠╣ ║ ║║ ║╠╦╝║║╣
          ╩╚═╚═╝╩ ╩╝╚╝  ╚  ╚═╝╚═╝╩╚═╩╚═╝      -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <!-- Fixed navbar -->
  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">My Cool Website</a>
      </div>
      <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
        <?php
        // Remember the directory names if there are sub-directories.
        $items =  array(
                    array('link'=>'/php-app-1/index.php', 'label'=>'Home'),
                    array('link'=>'/php-app-1/view.php', 'label'=>'View'),
                    //array('link'=>'/php-app-1/index.php', 'label'=>'Services'),
                    array('link'=>'#', 'label'=>'Contact'),
                  );
        $menu = '';
        foreach ($items as $val) {
            $class = ($_SERVER['PHP_SELF'] == $val['link']) ? ' class="active"' : '';
            $menu .= sprintf('<li'.$class.'><a href="%s">%s</a></li>', $val['link'], $val['label']);
        }
        $menu .= '</ul>';
        echo $menu;
        // echo "DEBUG: " . $_SERVER['PHP_SELF'] ;
        ?>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </nav>

  <body>
  <div class="container theme-showcase" role="main">
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <h1><?= isset($PageTitle) ? $PageTitle : "My Website"?></h1>
      <p><?= isset($PageDescription) ? $PageDescription : "My Company gives quality service and expert advice on These cool products. Call us now. Tel:(000) 000-0000" ?></p>
    </div>
