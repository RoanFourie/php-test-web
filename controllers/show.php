<?php

  // READ THE JSON FILE
  $url = './data.json';
  $json_string = file_get_contents($url);

  // DECODE THE JSON STRING INTO AN ARRAY
  $data = json_decode($json_string, true);

  // PROCESS THE JSON DATA
  foreach($data as $key => $value) {
    echo "<div class='panel panel-default'><div class='panel-heading'><h3 class='panel-title'>Name: ".$value['Name']."</h3>"."</div><div class='panel-body'>Age: ".$value['Age']."<br/>";
    $images_arr = $value['Pictures'];  // Get the images array into another array of strings
    if (empty($images_arr) or ($value['Pictures'] == "")){
      // blank entry, do nothing
      echo '<br/><p>This user have no images uploaded.</p><br/>';
    } else {
      // Else show each image
      foreach ($images_arr as $key => $val){
        echo '<img style="width:200px;height:200px;border:0" src="uploads/' . $val . '" alt="Image not found">';
      }
    }
    echo "</div></div>";
  }

// ╦═╗╔═╗╔═╗╔╗╔  ╔═╗╔═╗╦ ╦╦═╗╦╔═╗
// ╠╦╝║ ║╠═╣║║║  ╠╣ ║ ║║ ║╠╦╝║║╣
// ╩╚═╚═╝╩ ╩╝╚╝  ╚  ╚═╝╚═╝╩╚═╩╚═╝?>
