
<?php
  $var_user = "";

  if (isset($_POST['submit'])) {
    $var_user = $_POST['user_selected'];
    $j = 0;  // Variable for indexing uploaded image.
    $target_path = "./uploads/";  // For uploading images to folder.
    $store_path = "";  // For JSON file entry
    for ($i = 0; $i < count($_FILES['file']['name']); $i++) {
      // Loop to get individual element from the array
      $validextensions = array("jpeg", "jpg", "png"); // Extensions which are allowed.
      $ext = explode('.', basename($_FILES['file']['name'][$i])); // Explode file name from dot(.)
      $file_extension = end($ext); // Store extensions in the variable.
      $uid_string = md5(uniqid()) . "." . $ext[count($ext) - 1]; // Set the target path with a new unique name for the image.
      $target_path = $target_path . $uid_string;
      if ($i < (count($_FILES['file']['name']) - 1)){ // For JSON file
        $store_path = $store_path. $uid_string . ",";
      } else {
        $store_path = $store_path. $uid_string;  // Last image, don't add a comma
      }
      $j = $j + 1; // Increment the number of uploaded images according to the files in array.
      if (($_FILES["file"]["size"][$i] < 100000) // Approx. 100kb image filesize limit
      && in_array($file_extension, $validextensions)) { // filter extensions
        if (move_uploaded_file($_FILES['file']['tmp_name'][$i], $target_path)){
          // SUCCESS: If file moved to uploads folder.
          echo $j. ').'. $target_path . '<div class="alert alert-success" role="alert">Image uploaded successfully!.</div><br/><br/>';
        } else { // FAILURE: If File Was Not Moved.
          echo $j. ').'. $target_path . '<div class="alert alert-danger" role="alert">please try again!.</div><br/><br/>';
        }
      } else { // FAILURE: Filter type/size: If File Size And File Type Was Incorrect.
        echo $j. ').'. $target_path . '<div class="alert alert-danger" role="alert">***Invalid file Size or Type***</div><br/><br/>';
      }
      $target_path = "./uploads/";
    }

    // GET UPLOADED IMAGE NAMES INTO ARRAY
    $images_arr = str_getcsv($store_path);

    // READ THE JSON FILE INTO A JSON STRING
    $url = './data.json';
    $json_string = file_get_contents($url);

    // DECODE THE JSON STRING INTO ARRAY
    $data = json_decode($json_string, true);

    // GET PREVIOUS IMAGES
    foreach($data as $key => $value) {
      // if ($value['Name'] == "User2"){
      if ($value['Name'] == $var_user){
        //$prev_images_arr = str_getcsv($value['Pictures']);  // Get the images from csv string into an array of strings
        $prev_images_arr = $value['Pictures'];  // Get the images from csv string into an array of strings
      }
    }

    // ENCODE THE JSON DATA AND STORE INTO A NEW STRING
    foreach($data as $key => $value) {
      // if ($value['Name'] == "User2"){
      if ($value['Name'] == $var_user){
        if (empty($images_arr) or ($value['Pictures'] == "")){
          // blank entry, do nothing
          $data[$key]['Pictures'] = $images_arr;
          echo '<br/><p>This user have no previous images uploaded.</p><br/>';
        } else {
          $data[$key]['Pictures'] = array_merge($images_arr, $prev_images_arr);//Add old images(array) to new images(array) then store into a new array
        }
      }
    }
    $new_json_string = json_encode($data,JSON_UNESCAPED_UNICODE);

    // WRITE THE JSON DATA TO THE JSON FILE
    file_put_contents($url, $new_json_string);

  }
// ╦═╗╔═╗╔═╗╔╗╔  ╔═╗╔═╗╦ ╦╦═╗╦╔═╗
// ╠╦╝║ ║╠═╣║║║  ╠╣ ║ ║║ ║╠╦╝║║╣
// ╩╚═╚═╝╩ ╩╝╚╝  ╚  ╚═╝╚═╝╩╚═╩╚═╝?>
