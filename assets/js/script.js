var abc = 0;      // Declaring and defining global increment variable.
$(document).ready(function() {

  // Add new input file field dynamically, on click of "Add More Files" button.
  $('#add_more').click(function() {
    // Add More Files Button
    $(this).before($("<div/>", {
      id: 'filediv'
    }).fadeIn('slow').append($("<input/>", {
      name: 'file[]',
      type: 'file',
      id: 'file',
      class: 'well'
    }), $("<br/><br/>")));
  });

  // On change event of file input to select different file.
  $('body').on('change', '#file', function() {
  if (this.files && this.files[0]) {
    abc += 1; // Incrementing global variable by 1.
    var z = abc - 1;
    var x = $(this).parent().find('#previewimg' + z).remove();
    $(this).before("<div id='abcd" + abc + "' class='abcd'><img class='img-thumbnail' style='width: 200px; height: 200px' id='previewimg" + abc + "' src=''/></div>");
    var reader = new FileReader();
    reader.onload = imageIsLoaded;
    reader.readAsDataURL(this.files[0]);
    $(this).hide();
    // Delete Button
    $("#abcd" + abc).append($("<img/>", {
      id: 'img',
      src: 'x.png',
      alt: 'delete',
      class: 'btn btn-default'
    }).click(function() {
      $(this).parent().parent().remove();
    }));
  }
  });

  // Preview Image
  function imageIsLoaded(e) {
    $('#previewimg' + abc).attr('src', e.target.result);
  };
  $('#upload').click(function(e) {
    var name = $(":file").val();
    if (!name) {
      alert("First Image Must Be Selected");
      e.preventDefault();
    }
  });

});
