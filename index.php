<?php
$PageTitle = "My Page on my Website";
$PageKeywords = "";
$PageDescription = "This town deserves a better class of criminal and I'm gonna give it to them. Tell your men they work for me now. This is my city.";
$PageURL = "localhost:81/#";
include_once('./top.php');
?>

<div id="maindiv">
  <div id="formdiv">
    <h2>Multiple Image Upload Form</h2>
    <form enctype="multipart/form-data" action="" method="post">
      First Field is Compulsory. Only JPEG, PNG, JPG Type Images Uploaded. Image Size Should Be Less Than 100KB.
      <div class='panel panel-default'>
        <div class='panel-body'>
          <select id="user_selected" name="user_selected">
            <option value="0">--Select User--</option>
            <option value="User1">User 1</option>
            <option value="User2">User 2</option>
            <option value="User3">User 3</option>
          </select>
        </div>
      </div>
      <div id="filediv"><input name="file[]" type="file" id="file" class="well"/></div>
      <input type="button" id="add_more" class="btn btn-default" value="Add More Files"/>
      <input type="submit" value="Upload File" name="submit" id="upload" class="btn btn-default"/>
    </form>
    <!------- PHP Script here ------>
    <?php include "./controllers/upload.php"; ?>
  </div>
</div>

<?php include_once('./bottom.php');?>
